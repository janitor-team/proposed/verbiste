;; Copied from verbiste-el.emacsen-startup from
;; src:verbiste_0.1.45-3

;; entrypoint commands
(autoload 'verbiste-conjugate "verbiste"
  "Conjugate a VERB using Verbiste." t)
(autoload 'verbiste-deconjugate "verbiste"
  "Deconjugate a WORD using Verbiste." t)
